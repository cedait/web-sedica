from flask import Flask, jsonify, request, render_template, Blueprint, session, redirect
from services.sedica import Sedica
from views.auth import login_required

TipoPublicacion = Blueprint('TipoPublicacion', __name__)

sedica = Sedica()

@TipoPublicacion.route('/listar')
@login_required
def listar():
    session['sedica_currentMenu']='Administración'
    status,body = sedica.get('tiposPublicaciones')
    if status:
        return render_template('/tiposPublicaciones/listar.html', lista=body)
    return render_template('tiposPublicaciones/listar.html', lista=[],error=body)

@TipoPublicacion.route('/crear')
@login_required
def crear():
    return render_template('/tiposPublicaciones/crear.html')

@TipoPublicacion.route('/editar/<int:codigo>')
@login_required
def editar(codigo):
    status,body = sedica.get('tiposPublicaciones/'+str(codigo))
    if status:
        return render_template('tiposPublicaciones/editar.html', item=body)
    return render_template('/tiposPublicaciones/editar.html',item=None,error=body)

@TipoPublicacion.route('/guardar',methods=['POST'])
@login_required
def guardar():
    item = dict(request.values)
    item['codigo'] = int(item['codigo'])
    status,body = sedica.post('tiposPublicaciones/',item)
    if status:
        return redirect('listar')
    return render_template('/tiposPublicaciones/crear.html',item=None,error=body)

@TipoPublicacion.route('/actualizar',methods=['POST'])
@login_required
def actualizar():
    item = dict(request.values)
    cod = item['codigo']
    del item['codigo']
    status,body = sedica.put('tiposPublicaciones/'+cod,item)
    if status:
        return redirect('listar')
    return render_template('/tiposPublicaciones/editar.html',item=None,error=body)

@TipoPublicacion.route('/borrar/<int:codigo>',methods=['POST'])
@login_required
def borrar(codigo):
    status,body = sedica.delete('tiposPublicaciones/'+str(codigo))
    if status:
        return redirect('/administrarTiposPublicaciones/listar')
    return render_template('/tiposPublicaciones/listar.html',error=body)