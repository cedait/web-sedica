import os, dotenv
from flask import Flask, jsonify, request, render_template, Blueprint, session, redirect
from services.sedica import Sedica
from datetime import datetime
from views.auth import login_required

dotenv.load_dotenv()

Publicacion = Blueprint('Publicacion', __name__)

sedica = Sedica()

@Publicacion.route('/listar')
@login_required
def listar():
    session['sedica_currentMenu']='Administración'
    status,body = sedica.get('publicaciones')
    if status:
        return render_template('/publicaciones/listar.html', lista=body)
    return render_template('publicaciones/listar.html', lista=[],error=body)

@Publicacion.route('/crear',methods=['GET'])
@login_required
def crear():
    status_tp,body_tp = sedica.get('tiposPublicaciones')
    status_c,body_c = sedica.get('cadenas')
    if status_tp and status_c:
        return render_template('/publicaciones/crear.html', tipos=body_tp, cadenas=body_c)
    return render_template('publicaciones/listar.html', lista=[],error={**body_tp,**body_c})

@Publicacion.route('/editar/<int:codigo>')
@login_required
def editar(codigo):
    status,body = sedica.get('publicaciones/'+str(codigo))
    status_tp,body_tp = sedica.get('tiposPublicaciones')
    status_c,body_c = sedica.get('cadenas')
    if status and status_tp and status_c:
        body['etiquetas'] = str(body['etiquetas'])[1:-1].replace("'",'').replace(', ',',')
        body['fecha'] = formatFecha(body['fecha'])
        return render_template('publicaciones/editar.html', item=body, tipos=body_tp, cadenas=body_c)
    return render_template('/publicaciones/editar.html',item=None,error=body)

@Publicacion.route('/guardar',methods=['POST'])
@login_required
def guardar():
    item = dict(request.values)
    item['cadena'] = int(item['cadena'])
    item['tipo'] = int(item['tipo'])
    item['codigo'] = int(item['codigo'])
    item['usuario'] = session['sedica_user']['identificacion']
    # item['etiquetas'] = item['etiquetas'].split(',')
    if str(item['etiquetas'])=='':
        item['etiquetas'] = []
    else:
        item['etiquetas'] = formatLabels(item['etiquetas'])

    guardarArchivo(request, 'archivo',['.pdf'],item)
    guardarArchivo(request, 'imagen',['.jpg','.png','.jpeg'],item)

    status,body = sedica.post('publicaciones/',item)
    if status:
        return redirect('listar')
    return render_template('/publicaciones/crear.html',item=None,error=body)

@Publicacion.route('/actualizar',methods=['POST'])
@login_required
def actualizar():
    item = dict(request.values)
    item['cadena'] = int(item['cadena'])
    item['tipo'] = int(item['tipo'])
    cod = item['codigo']

    # Guardar archivo e imagen si existe 
    if 'archivo' in request.files:
        guardarArchivo(request, 'archivo',['.pdf'],item)

    if 'imagen' in request.files:
        guardarArchivo(request, 'imagen',['.jpg','.png','.jpeg'],item)

    # Formatear etiquetas
    if str(item['etiquetas'])=='':
        item['etiquetas'] = []
    else:
        item['etiquetas'] = formatLabels(item['etiquetas'])



    del item['codigo']
    status,body = sedica.put('publicaciones/'+cod,item)
    if status:
        return redirect('listar')
    return render_template('/publicaciones/editar.html',item=None,error=body)

@Publicacion.route('/borrar/<int:codigo>',methods=['POST'])
@login_required
def borrar(codigo):
    status,body = sedica.delete('publicaciones/'+str(codigo))
    if status:
        return redirect('/administrarPublicaciones/listar')
    return render_template('/publicaciones/listar.html',error=body)


def formatFecha(fecha):
    fecha=fecha.replace(' GMT','')
    formato = '%a, %d %b %Y %H:%M:%S'
    '''now=datetime.strftime(datetime.now(), formato)'''
    fecha = datetime.strptime(fecha, formato)
    # Formateo para el api y la bd
    formato2 = '%Y-%m-%d'
    fecha_formateada=datetime.strftime(fecha,formato2)  
    return fecha_formateada

def formatLabels(etiquetas):
    if etiquetas is None:
        return None
    elif etiquetas == '[]':
        return []
    return (etiquetas.replace('"','').split(','))

def guardarArchivo(request, nombre, extension, item):
    if nombre not in request.files:
        return render_template('/publicaciones/crear.html',item=None,error='No hay {} {}'.format(nombre,extension))
    
    archivo = request.files[nombre]  
    if (archivo.filename):
        ext = '.'+archivo.filename.split('.')[1]
    else:
        return render_template('/publicaciones/crear.html',item=None,error='Error al cargar el nombre del archivo')

    if ext not in extension: 
       return render_template('/publicaciones/crear.html',item=None,error='Extension de archivo incorrecta: '+str(ext)+' Solo se permiten archivos'+str(extension))

    filename='{}_{}{}'.format(item['codigo'],item['nombre'],ext)
    filename=filename.replace(' ','').replace('|','_')
   
    path=os.path.join(os.getcwd()+os.getenv("UPLOAD_FOLDER"),filename)
    
    archivo.save(path)
    item[nombre] = filename
