from flask import Flask, jsonify, request, render_template, Blueprint, session, redirect
from services.sedica import Sedica

Pub = Blueprint('Pub', __name__)

sedica = Sedica()

@Pub.route('/')
@Pub.route('/<tipoPub>')
def verTodas(tipoPub=None):
    session['sedica_currentMenu']='Publicaciones'
    session['sedica_cadenaPub']=tipoPub
    status_c,body_c = sedica.get('cadenas')
    status_tp,body_tp = sedica.get('tiposPublicaciones')
    status,body = sedica.get('publicaciones')
    if status and status_c and status_tp:
        return render_template('/publicaciones/index.html', lista=body, cadenas=body_c, tipos=body_tp)
    return render_template('publicaciones/index.html', lista=[],error=body)

@Pub.route('/etiqueta/<tag>')
def verPorEtiqueta(tag):
    session['sedica_currentMenu']='Publicaciones'
    session['sedica_cadenaPub']=''
    session['sedica_etiqueta']=tag
    status_c,body_c = sedica.get('cadenas')
    status_tp,body_tp = sedica.get('tiposPublicaciones')
    status,body = sedica.get('publicaciones/etiqueta/'+tag)
    if status and status_c and status_tp:
        return render_template('/publicaciones/index.html', lista=body, cadenas=body_c, tipos=body_tp)
    return render_template('publicaciones/index.html', lista=[],error=body)

@Pub.route('/tipo/<int:codigo>')
def verPorTipo(codigo):
    session['sedica_currentMenu']='Publicaciones'
    session['sedica_cadenaPub']=''
    session['sedica_etiqueta']=''
    status_c,body_c = sedica.get('cadenas')
    status_tp,body_tp = sedica.get('tiposPublicaciones')
    status,body = sedica.get('publicaciones/tipo/'+str(codigo))
    if status and status_c and status_tp:
        return render_template('/publicaciones/index.html', lista=body, cadenas=body_c, tipos=body_tp)
    return render_template('publicaciones/index.html', lista=[],error=body)

@Pub.route('/cadena/<int:codigo>')
def verPorCadena(codigo):
    session['sedica_currentMenu']='Publicaciones'
    session['sedica_cadenaPub']=codigo
    session['sedica_etiqueta']=''
    status_c,body_c = sedica.get('cadenas')
    status_tp,body_tp = sedica.get('tiposPublicaciones')
    status,body = sedica.get('publicaciones/cadena/'+str(codigo))
    if status and status_c and status_tp:
        return render_template('/publicaciones/index.html', lista=body, cadenas=body_c, tipos=body_tp)
    return render_template('publicaciones/index.html', lista=[],error=body)