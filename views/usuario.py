from flask import Flask, jsonify, request, render_template, Blueprint, session, redirect
from services.sedica import Sedica
from views.auth import login_required

Usuario = Blueprint('Usuario', __name__)

sedica = Sedica()

@Usuario.route('/listar')
@login_required
def listar():
    session['sedica_currentMenu']='Administración'
    status,body = sedica.get('usuarios')
    if status:
        return render_template('/usuarios/listar.html', lista=body)
    return render_template('usuarios/listar.html', lista=[],error=body)

@Usuario.route('/crear')
@login_required
def crear():
    return render_template('/usuarios/crear.html')

@Usuario.route('/editar/<int:identificacion>')
@login_required
def editar(identificacion):
    status,body = sedica.get('usuarios/'+str(identificacion))
    if status:
        return render_template('usuarios/editar.html', item=body)
    return render_template('/usuarios/editar.html',item=None,error=body)

@Usuario.route('/actualizarPerfil/<int:identificacion>')
@login_required
def actualizarPerfil(identificacion):
    session['sedica_currentMenu']='Mi Cuenta'
    status,body = sedica.get('usuarios/'+str(identificacion))
    if status:
        return render_template('usuarios/actualizar.html', item=body)
    return render_template('/usuarios/actualizar.html',item=None,error=body)

@Usuario.route('/guardar',methods=['POST'])
@login_required
def guardar():
    item = dict(request.values)
    item['estado'] = 'Activo'
    status,body = sedica.post('usuarios/',item)
    if status:
        return redirect('listar')
    return render_template('/usuarios/crear.html',item=None,error=body)

@Usuario.route('/actualizar',methods=['POST'])
@login_required
def actualizar():
    item = dict(request.values)
    id = item['identificacion']
    del item['identificacion']
    # del item['correo']
    status,body = sedica.put('usuarios/'+id,item)
    if status:
        if session['sedica_currentMenu']=='Administración':
            return redirect('listar')
        return redirect('/home')
    return render_template('/usuarios/editar.html',item=None,error=body)

@Usuario.route('/borrar/<int:identificacion>',methods=['POST'])
@login_required
def borrar(identificacion):
    status,body = sedica.delete('usuarios/'+str(identificacion))
    if status:
        return redirect('/administrarUsuarios/listar')
    return render_template('/usuarios/listar.html',error=body)