from flask import Flask, jsonify, request, render_template, Blueprint, session, redirect
from services.sedica import Sedica
from views.auth import login_required

Cadena = Blueprint('Cadena', __name__)

sedica = Sedica()

@Cadena.route('/listar')
@login_required
def listar():
    session['sedica_currentMenu']='Administración'
    status,body = sedica.get('cadenas')
    if status:
        return render_template('/cadenas/listar.html', lista=body)
    return render_template('cadenas/listar.html', lista=[],error=body)

@Cadena.route('/crear')
@login_required
def crear():
    return render_template('/cadenas/crear.html')

@Cadena.route('/editar/<int:codigo>')
@login_required
def editar(codigo):
    status,body = sedica.get('cadenas/'+str(codigo))
    if status:
        return render_template('cadenas/editar.html', item=body)
    return render_template('/cadenas/editar.html',item=None,error=body)

@Cadena.route('/guardar',methods=['POST'])
@login_required
def guardar():
    item = dict(request.values)
    item['codigo'] = int(item['codigo'])
    status,body = sedica.post('cadenas/',item)
    if status:
        return redirect('listar')
    return render_template('/cadenas/crear.html',item=None,error=body)

@Cadena.route('/actualizar',methods=['POST'])
@login_required
def actualizar():
    item = dict(request.values)
    cod = item['codigo']
    del item['codigo']
    status,body = sedica.put('cadenas/'+cod,item)
    if status:
        return redirect('listar')
    return render_template('/cadenas/editar.html',item=None,error=body)

@Cadena.route('/borrar/<int:codigo>',methods=['POST'])
@login_required
def borrar(codigo):
    status,body = sedica.delete('cadenas/'+str(codigo))
    if status:
        return redirect('/administrarCadenas/listar')
    return render_template('/cadenas/listar.html',error=body)