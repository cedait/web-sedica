from flask import session, request, redirect, Blueprint, render_template
from functools import wraps
import os, dotenv, requests, jwt
from services.sedica import Sedica

dotenv.load_dotenv()

api = os.getenv("SEDICA_API_PATH")

Auth = Blueprint('auth', __name__)

sedica = Sedica()

def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not 'sedica_user' in session.keys():
            return render_template('login.html'), 200
        if session['sedica_user'] is None:
            return render_template('login.html'), 200
        return f(*args, **kwargs)
    return decorated_function

def logout_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not 'sedica_user' in session.keys():
            return f(*args, **kwargs)
        if session['sedica_user'] is None:
            return f(*args, **kwargs)
        session['sedica_currentMenu']='Inicio'
        return render_template('index.html'), 200
    return decorated_function


@Auth.route('/login',methods=['GET','POST'])
@logout_required
def login():
    usuario = dict(request.values)
    if request.method == 'GET':
        return render_template('login.html'), 200
    # Loguear
    res = requests.post(api+'auth/login',json=usuario)
    status,body = res.status_code, res.json()
    if ((status//200) == 1):
        session['sedica_user'] = None
        session['headers'] = None
        # Obtener token
        token = body['access_token']
        # Setear usuario de la session
        sedica_user = jwt.decode(token,os.getenv("SEDICA_JWT_KEY"))['identity']
        session['sedica_user'] = sedica_user
        session['sedica_currentMenu']='Inicio'
        # Definir objeto request para realizar peticiones  al API 
        session['headers'] = {'Authorization': 'Bearer ' + token}        
        session.permanent = True

        return render_template('index.html'), 200
    if 'msg' in body.keys():
        return render_template('login.html',error=body['msg']), 401
    return render_template('login.html', error="Ocurrio un error loguandose: "+str(body['error']) ), 400
    
@Auth.route('/logout')
@login_required
def logout():
    session['sedica_user'] = None
    session['headers'] = None
    session['sedica_currentMenu']='Inicio'
    return render_template('login.html'), 200