from flask import Flask, request, render_template, session
from flask_session import Session
import datetime 
import os, dotenv
from views.publicacion import Publicacion
from views.tipoPublicacion import TipoPublicacion
from views.cadena import Cadena
from views.usuario import Usuario
from views.pub import Pub
from views.auth import Auth

dotenv.load_dotenv()

app = Flask(__name__, template_folder='templates', static_url_path='/static')

@app.route('/')
@app.route('/home')
@app.route('/home/')
def home():
    session['sedica_currentMenu']='Inicio'
    return render_template('/index.html')

@app.route('/login')
@app.route('/login/')
def login():
    session['sedica_currentMenu']='Iniciar Sesión'
    return render_template('/login.html')

app.register_blueprint(Publicacion, url_prefix='/administrarPublicaciones')
app.register_blueprint(TipoPublicacion, url_prefix='/administrarTiposPublicaciones')
app.register_blueprint(Cadena, url_prefix='/administrarCadenas')
app.register_blueprint(Usuario, url_prefix='/administrarUsuarios')
app.register_blueprint(Pub, url_prefix='/publicaciones')
app.register_blueprint(Auth, url_prefix='/auth')

'''ERRORS'''
@app.errorhandler(404)
def page_not_found(e):
    return 'Error: Endpoint no hallado.', 404

@app.errorhandler(405)
def method_not_allow(e):
    return 'Error: Método no permitido.',  405

@app.errorhandler(500)
def handle_500(e):
    return 'Error: del sistema en el servidor.', 500

@app.errorhandler(Exception)
def handle_exception(e):
    return 'Error: Ha ocurrido un error al ejecutar el servidor, si es necesario contacte al administrador. '+str(e), 500 
'''END ERRORS'''


@app.before_request
def make_session_permanent():
    session.modified = True
    session.permanent = True
    app.permanent_session_lifetime = datetime.timedelta(minutes=30)

if __name__ == '__main__':
    #Variables de sesión
    app.secret_key = 'OCML3BRawWEUeaxcuKHLpw'

    # Sesion login usuario config   
    app.config['SESSION_PERMANENT'] = True
    app.config['SESSION_TYPE'] = 'filesystem'
    app.config['PERMANENT_SESSION_LIFETIME'] = datetime.timedelta(minutes=30)
    app.config['SESSION_FILE_THRESHOLD'] = 100 

    #Iniciar sesion de login usuario
    sess = Session() 
    ''''sess.init_app(app)'''

    app.run(port=8000,debug=True)