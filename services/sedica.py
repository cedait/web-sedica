import os, dotenv, requests
from flask import session

dotenv.load_dotenv()

api = os.getenv("SEDICA_API_PATH")

class Sedica():

    def get(self,endpoint):
        res = requests.get(api+endpoint,headers=getHeaders())
        status, body = res.status_code, res.json()
        if ((status//200) == 1):
            return True, body
        return False, body

    def post(self,endpoint,body_json):
        res = requests.post(api+endpoint, json=body_json,headers=getHeaders())
        status, body = res.status_code, res.json()
        if ((status//200) == 1):
            return True, body
        return False, body

    def put(self,endpoint,body_json):
        res = requests.put(api+endpoint, json=body_json,headers=getHeaders())
        status, body = res.status_code, res.json()
        if ((status//200) == 1):
            return True, body
        return False, body

    def delete(self,endpoint):
        res = requests.delete(api+endpoint,headers=getHeaders())
        status, body = res.status_code, res.json()
        if ((status//200) == 1):
            return True, body
        return False, body

def getHeaders():
    if 'headers' not in session.keys():
        return None
    return session['headers']